import examples from './examples';
import description from './tabs.md';

export default {
  description,
  examples,
  bootstrapComponent: 'b-tabs',
  followsDesignSystem: true,
};
